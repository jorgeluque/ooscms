$(document).ready(function(){

    $('#login_usuario').submit(function(event){
        event.preventDefault();
        $('#error-login').html('');
        $.ajax({
            url: base_url + 'login/validar_login',
            type: 'post',
            data: {
            email: $('#email').val(),
            password: md5($('#password').val())
            },
            success: function(html){
            if(html == 'ok')
                window.location.replace(base_url);
            else
                $('#error-login').html('<p class="bg-danger">'+html+'</p>');
            }
        });
    });

});

