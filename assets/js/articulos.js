$(document).ready(main);

function main () {
    
    // Si estamos en la ficha de edicion o añadido de un articulo
    if ($(".gc-add").length || $(".gc-edit").length) {

        // Preparamos los telefonos para el add-edit
        prepararTelefonosAddEdit();

    }
    // Si estamos en modo lectura de un articulo
    else if ($(".gc-read").length) {
        
        // Preparamos los telefonos para el read
        prepararTelefonosRead();

    }
    
}

function prepararTelefonosAddEdit () {

    // Si existe el campo de telefonos
    if ($("#field-telefonos").length) {

        // Mostramos el form de telefonos
        mostrarFormTelefonos();

        // Incluimos los telefonos iniciales
        incluirTelefonosIniciales(true);

        // Comprobamos si hay que deshabilitar o no el form de nuevo telefono
        comprobarFormDisabled();

        // Creamos los controles de los telefonos
        controlTelefonosFicha();

    }

}

function prepararTelefonosRead () {

    // Si existe el campo de telefonos
    if ($("#field-telefonos").length) {

        // Mostramos el contenedor de telefonos
        mostrarContenedorTelefonosRead();

        // Incluimos los telefonos iniciales
        incluirTelefonosIniciales(false);

    }

}

function mostrarFormTelefonos () {

    // Abrimos el contenedor principal
    var html = "<div id='telefonos-ficha'>";

    // Incluimos el formulario para añadir telefono
    html += "<div id='telefono-nuevo'>" +
                "<div class='col-xs-12'>" +
                    "<h3>TELÉFONOS</h3>" +
                "</div>" +
                "<div class='col-xs-3'>" +
                    "<input type='text' id='telefono-numero' class='form-control' placeholder='Teléfono' disabled>" +
                "</div>" +
                "<div class='col-xs-3'>" +
                    "<input type='text' id='telefono-titulo' class='form-control' placeholder='Título' disabled>" +
                "</div>" +
                "<div class='col-xs-4'>" +
                    "<input type='text' id='telefono-descripcion' class='form-control' placeholder='Descripción' disabled>" +
                "</div>" +
                "<div class='col-xs-2'>" +
                    "<button type='button' id='telefono-guardar' class='btn btn-primary' disabled>Añadir</button>" +
                "</div>" +
            "</div>";

    // Incluimos el contenedor de los telefonos
    html += "<div id='telefonos-listado'></div>";

    // Cerramos el contenedor principal
    html += "</div>";

    // Incluimos el html
    $(html).insertAfter($("#field-telefonos"));

}

function incluirTelefonosIniciales (botonBorrar) {

    var i;

    // Recogemos los telefonos del articulo
    var telefonos = recogerTelefonosInicialesArticulo();
    
    // Si tiene telefonos
    if (telefonos.length > 0) {

        // Nos recorremos los telefonos
        for (i=0; i<telefonos.length; i++) {
            
            // Incluimos el telefono en el listado
            incluirTelefonoListado(telefonos[i], botonBorrar);

        }

    }

}

function incluirTelefonoListado (telefono, botonBorrar) {

    // Abrimos el contenedor principal
    var html = "<div class='col-xs-4 contenedor-telefono'><div class='telefono'>";

    // Definimos el boton de borrar si es requerido
    var htmlBorrar = botonBorrar ? "<button type='button' class='telefono-borrar'><i class='fa fa-trash'></i></button>" : "";

    // Incluimos el formulario para añadir telefono
    html += "<div class='telefono-titulo'>" +
                "<h2>" + telefono.titulo + "</h2>" +
            "</div>" +
            "<div class='telefono-numero'>" +
                "<p>" + telefono.numero + "</p>" +
            "</div>" +
            "<div class='telefono-descripcion'>" +
                "<p>" + telefono.descripcion + "</p>" +
            "</div>" +
            htmlBorrar + 
            "<input type='hidden' class='telefono-json' value='" + JSON.stringify(telefono) + "'>";  
    
    // Cerramos el contenedor principal
    html += "</div></div>";

    // Incluimos el telefono
    $("#telefonos-listado").append(html);

}

function comprobarFormDisabled () {

    // Recogemos la cantidad de telefonos que tiene definidos
    var cantidadTelefonos = $("#telefonos-listado .telefono").length;

    // Si tiene menos de 5
    if (cantidadTelefonos < 5) {

        // Habilitamos el form
        $("#telefono-nuevo").find("input").prop("disabled", false);
        $("#telefono-guardar").prop("disabled", false);

    }
    // Si hay 5 o mas
    else {

        // Deshabilitamos el form
        $("#telefono-nuevo").find("input").prop("disabled", true);
        $("#telefono-guardar").prop("disabled", true);

    }

}

function controlTelefonosFicha () {

    // Al pulsar en guardar un telefono
    $("#telefono-guardar").on("click", function(){

        // Recogemos los datos del telefono
        var telefono = {
            "numero" : $("#telefono-numero").val(),
            "titulo" : $("#telefono-titulo").val(),
            "descripcion" : $("#telefono-descripcion").val()
        };

        // Comprobamos que el numero no este vacio
        if (telefono.numero != "") {

            // Incluimos el telefono en el listado
            incluirTelefonoListado(telefono, true);

            // Limpiamos el form de nuevo telefono
            limpiarFormNuevoTelefono();
            
            // Comprobamos si hay que deshabilitar o no el form de nuevo telefono
            comprobarFormDisabled();

            // Generamos el json de los telefonos para guardarlo en el input hidden
            generarJsonTelefonos();

        }
        // Si el numero esta vacio, hacemos focus en el campo
        else $("#telefono-numero").focus();

    });

    // Al pulsar en borrar telefono
    $(document).on("click", ".telefono-borrar", function(){

        // Borramos el telefono
        $(this).closest(".contenedor-telefono").remove();

        // Comprobamos si hay que deshabilitar o no el form de nuevo telefono
        comprobarFormDisabled();

        // Generamos el json de los telefonos para guardarlo en el input hidden
        generarJsonTelefonos();

    });

}

function recogerTelefonosInicialesArticulo () {

    // Recogemos los telefonos en formato string
    var telefonos = $("#field-telefonos").val();

    // Si no tiene telefonos, los definimos como array vacio
    if (telefonos == "") telefonos = [];

    // Si tiene telefonos, los decodificamos, porque es un json
    else telefonos = $.parseJSON(telefonos);

    return telefonos;

}

function generarJsonTelefonos () {

    // Por defecto
    var telefonos = [];
    var telefono;

    // Nos recorremos los telefonos del form
    $("#telefonos-listado .telefono").each(function(){

        // Recogemos el telefono
        telefono = $(this).find(".telefono-json").val();

        // Parseamos el telefono
        telefono = $.parseJSON(telefono);

        // Solo añadiremos los primeros 5
        if (telefonos.length < 5) {

            // Añadimos el telefono al array
            telefonos.push(telefono);

        }

    });

    // Transformamos el array de telefonos en json
    telefonos = JSON.stringify(telefonos);

    // Asignamos el valor de los telefonos en el hidden
    $("#field-telefonos").val(telefonos);

}

function limpiarFormNuevoTelefono () {

    // Limpiamos los input
    $("#telefono-nuevo").find("input").val("");

}

function mostrarContenedorTelefonosRead () {

    // Abrimos el contenedor principal
    var html = "<div id='telefonos-ficha'>";

     // Incluimos el titulo
    html += "<div class='col-xs-12'>" +
                "<h3>TELÉFONOS</h3>" +
            "</div>";

    // Incluimos el contenedor de los telefonos
    html += "<div id='telefonos-listado'></div>";

    // Cerramos el contenedor principal
    html += "</div>";

    // Incluimos el html
    $(html).insertAfter($("#telefonos_form_group"));

    // Adicionalmente, eliminamos el campo de los telefonos original con el json
    $("#telefonos_form_group").remove();
    
}