function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function confirmarBajaAbonado(idAbonado)
{
    $('#myModalLabel').text('¡Aviso Importante!');
    $('.modal-body').text('¿Estás seguro que deseas dar de baja el abonado? Si das de baja el abonado no se mostrará en Goonce.');
    $('#myModal').modal('show');
    $('#confirmButton').click(function(){
      $('#confirmButton').prop("disabled", true);
      bajaAbonado(idAbonado)
    });
}

function bajaAbonado(idAbonado)
{
   $.ajax({
      url: base_url + 'admin/abonados/baja',
      type: 'post',
      data: {
        idAbonado: idAbonado
      },
      success: function(html){
        $('.modal-body').append('<p class="bg-success" style="padding: 5px 10px;margin-top: 25px;">El abonado se ha dado de baja satisfactoriamente.</p>');
        setTimeout(function(){
          $('#cerrarButton').trigger('click');
        }, 1000);
        setTimeout(function(){
          window.location.href = site_url + "admin/abonados";
        }, 1000);
      }

  });
}


function loadMunicipios(){
  $.ajax({
      url: base_url + "admin/municipios/get",
      type: 'post',
      datatype: 'json',
      data: {
        idProvincia: $('#field-provincia').val()
      },
      success: function(html){
        $('#field-localidad').html(html);
      }
    });
}

function click_actividad(idActividad, nombre_actividad)
{
  $('#field-actividad').val(nombre_actividad);
  $('#search-results-actividad').addClass('hidden');
  $('#id-actividad').val(idActividad);

  $.ajax({
    url: base_url + "admin/actividades/getCategoria",
    type: 'post',
    dataType: 'json',
    data: {
      idActividad: idActividad
    },
    success: function(json){
      $('#field-id-categoria').val(json.idCategoria);
      $('#field-categoria').val(json.nombre);
    }
  });
}

function validarFormulario()
{
  errores = 0;
  $('.input-group').removeClass('has-error');
  $('#report-error').hide();

  // Validamos si éxiste un campos vacios.
  $('input[required]').each(function(){
    if($(this).val() == '')
    {
        $(this).parent().fadeIn('slow').addClass('has-error');
        ++errores;
    }
  });

  // Si hay errores se muestra el mensaje de error, si no, se valida los datos por ajax.
  if(errores != 0)
  {
    $('#report-error').html('Existen campos sin completar. Por favor, complete los campos requeridos.');
    $('#report-error').fadeIn();
  }
  else
  {
    $.ajax({
      url: base_url + 'admin/abonados/validate',
      type: 'post',
      data: $('#formAbonado').serialize(),
      success: function(respuesta){
        if(respuesta == 'true')
        {

        }
        else
        {
          $('#report-error').html(respuesta);
          $('#report-error').fadeIn();
        }
      }
    });
  }
}

  $('#selectPrefijoPais').change(function() {
    var $option = $(this).find('option:selected');
    //Added with the EDIT
    var value = $option.val();//to get content of "value" attrib
    var text = $option.text();//to get <option>Text</option> content
    $('#paisAbonado').val(text);
});


function confirmaEliminarTelefono(i)
  {
    $('#myModalLabel').text('¡Aviso Importante!');
    $('.modal-body').text('Si elimina el teléfono, se eliminará directamente en la base de datos. ¿Estas seguro que desea eliminar el teléfono?');
    $('#myModal').modal('show');
    $('#confirmButton').click(function(){eliminarTelefono(i)});
  }


function eliminarTelefono(i)
{
  // $.ajax({
  //   url: base_url + 'admin/telefonos/delete',
  //   type: 'post',
  //   data: {
  //     telefono: $('#field-telefono-'+i).val(),
  //     idAbonado: $('#idAbonado').val()
  //   },
  //   success: function(html){
      $('#form-telefono-'+ i).remove();
      $('#cerrarButton').trigger('click');
    // }

  // });
}

function addTelefono()
{
  var n = $('#telefonos_form_group').children().length;
  if(n == 0)
  {
    $.ajax({
      url: base_url + 'admin/telefonos/form',
      type: 'post',
      data: {
        posicion: posicion
      },
      success: function(html){
        $('#telefonos_form_group').append(html);
        $('#totalTelefonos').val(posicion);
      }
    });
  }
  else
  {
    var telefono = $('#field-telefono-'+(n-1)).val();
    var posicion = $('#telefonos_form_group').children('div:last').attr('id').substr(-1);
    posicion++;
    $('#field-telefono-'+n).parent().removeClass('has-error');

    if(telefono == '')
    {
      $('#field-telefono-'+n).parent().addClass('has-error');
    }
    else
    {
      $.ajax({
        url: base_url + 'admin/telefonos/form',
        type: 'post',
        data: {
          posicion: posicion
        },
        success: function(html){
          $('#telefonos_form_group').append(html);
          $('#totalTelefonos').val(posicion);
        }
      });
    }
  }
}


function verHistorialTelefonos(idAuto, idAbonado)
{
  $.ajax({
    url: base_url + 'ajax/verhistorialTelefonos',
    type: 'post',
    data: {
      idAuto: idAuto,
      idAbonado: idAbonado
    },
    success: function(html) {
      $('.modal-dialog').css("width", '1024px');
      $('#confirmButton').hide();
      $('#myModalLabel').text('Historial Telefonos');
      $('.modal-body').html(html);
      $('#myModal').modal('show');
    }
  });


}
