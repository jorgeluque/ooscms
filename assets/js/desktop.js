//---------------------------------------------------------------------------------
// paginacion
function mostrarPagina(provincia,pagina){
   $.ajax({
        url: URL+'public/ajax/mostrarPagina.php',
        type: 'post',
        dataType: 'JSON',
        data: {
            provincia:provincia,
            pagina: pagina
        },
        success: function(json){
            $('.oficinas').html(json.oficinas);
            $('.paginador').html(json.paginacion);
            window.history.pushState("", "Oficinas", json.pagina);
        }
    });
}
//---------------------------------------------------------------------------------
// guardamos comentario en BD
function saveComentario(pagina){
   $.ajax({
        url: URL+'public/ajax/saveComentario.php',
        type: 'post',
        data: {
            pagina: pagina,
            alias: $('#alias').val(),
            texto: $('#texto').val()
        },
        success: function(result){
            $('#alias').val('');
            $('#texto').val('');
            $('#answer').html(result);
            $('#answer').css("display","block");
        }
    });
}
//---------------------------------------------------------------------------------
// busqueda
function busqueda(){
    window.location.href = URL+"busqueda/"+$('.texto-busqueda').val();
}
//---------------------------------------------------------------------------------
$(document).keypress(function(e) {
    if(e.which == 13) {
        if (document.activeElement.name == "texto-busqueda"){
            busqueda();
        }
    }
});
//---------------------------------------------------------------------------------
// busqueda
function cambiarPaginaBusqueda(busqueda,pagina){
    window.location.href = URL+"busqueda/"+busqueda+"/"+pagina;
}
//---------------------------------------------------------------------------------
function redirectSearchGoogle()
{
  var busqueda = $('#input-search-google').val();
  if(busqueda != '')
  {
    $.ajax({
    url: URL + 'ajax/searchByGoogle',
    type: 'POST',
    data: {
      busqueda: busqueda,
      idLlamada: $('#idLlamada').val()
    },
    success: function(url){
      window.open(url);
    }
  });
  }
}
//---------------------------------------------------------------------------------
function mostrar_ocultar_buscador(nameButton, type) {
    var estadoBtn = $('#'+nameButton).html();

    $('.buscador').hide();
    $('.btn-buscador').html('Mostrar');

    if(estadoBtn == 'Mostrar'){
        $('#buscador-'+type).show();
        $('#'+nameButton).html('Ocultar');
    }else{
        $('#'+nameButton).html('Mostrar');
    }
}
//---------------------------------------------------------------------------------
function showDescription(id, mostrar){
    if(mostrar == 1){
        $('#mas-informacion-' + id).html('<a href="#" onclick="showDescription(' + id + ',0);return false;">(+) Más información</a>');
        $('#descripcion-' + id).show();
    }else if(mostrar == 0){
        $('#mas-informacion-' + id).html('<a href="#" onclick="showDescription(' + id + ',1);return false;">(-) Menos información</a>');
        $('#descripcion-' + id).hide();
    }
}
//---------------------------------------------------------------------------------
var fechaHoraInicio;
if($('#inicioLlamada').val() == "1970-01-01 01:00:00")
    fechaHoraInicio = new Date();
else
{
  fechaHoraInicio_aux = $('#inicioLlamada').val().toString().replace('-','/').replace('-','/');

  year = fechaHoraInicio_aux.substring(0,4);
  month = fechaHoraInicio_aux.substring(5,7);
  day = fechaHoraInicio_aux.substring(8,10);
  hour = fechaHoraInicio_aux.substring(11,13);
  min = fechaHoraInicio_aux.substring(14,16);
  sec = fechaHoraInicio_aux.substring(17,19);

  fechaHoraInicio = new Date(year, month, day, hour, min, sec);
}


function duracionLlamada() {
  var fechaHoraActual = new Date();
  var diffFechaHora = new Date(fechaHoraActual-fechaHoraInicio);

  var horas = diffFechaHora.getHours()-2; //Si duración llamada cambia, sumar o restar un 1 a la hora dependiendo de la franja horaria.
  var minutos = diffFechaHora.getMinutes();
  var segundos = diffFechaHora.getSeconds();

  return (horas*60*60) + (minutos*60) + segundos;
}
//---------------------------------------------------------------------------------
function printDuracionLlamada(){
    var duracionLlamadaSec = duracionLlamada();
    $('#duracionLlamada').val(convertSecondsToTime(duracionLlamadaSec));
    if(duracionLlamadaSec > 120)
        $('#duracionLlamada').css('color', 'red');
}

window.onload = function() {
  setInterval(printDuracionLlamada, 1000);
  setInterval(printCosteLlamada, 1000);
}
//---------------------------------------------------------------------------------
function convertSecondsToTime(time){
    var hours = Math.floor( time / 3600 );
    var minutes = Math.floor( (time % 3600) / 60 );
    var seconds = time % 60;

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds;  // 2:41:30
}
//---------------------------------------------------------------------------------
function costeLlamada() {
    var segundosLlamada = this.duracionLlamada();

    var callType = $("#callType").val();
    var euros = 0;
    var establecimiento = 0.28117975;

    switch(callType) {
        case 'p11':
            euros = 1.9835;
            break;
        case 'p50':
            euros = 1.9835;
            break;
        case 'p43':
            euros = 2.4793;
            break;
        case 'p60':
            euros = 2.9752;
            break;
        case 'p80':
            euros = 2.9752;
            break;
    }
    coste = establecimiento + (euros / 60) * (segundosLlamada - 11);
    return Math.round(coste * 100) / 100;
}
//---------------------------------------------------------------------------------
function printCosteLlamada(){
    var coste = costeLlamada();
    $('#costeLlamada').val(coste);
}
//---------------------------------------------------------------------------------
function mostrarLightboxSms(idAbonado, numero){
    $('#texto-sms').html('');
    $('#telefono-movil').val('');
    $('#texto-sms').val('');
    $('#sms-idAbonado').val(idAbonado);
    $('#sms-telefonoAbonado').val(numero);
    if($('#box-sms').is(":visible")){
            $('#box-sms').hide();
    }else{
        $.ajax({
            url: URL + 'ajax/getTextoSMSAbonado',
            type: 'POST',
            dataType: 'JSON',
            data: {
                idAbonado: idAbonado,
                numero: numero,
                idLlamada: $('#idLlamada').val()
            },
            success: function(json){
                if(json.sms != '')
                    $('#texto-sms').val(json.sms);
                if(json.movil != '')
                    $('#telefono-movil').val(json.movil);
            }
        });
        var position = $('#envia-sms-' +  idAbonado+numero).position();
        var top = position.top + 28;
        var left = position.left - 110;
        $('#box-sms').css('top', top + 'px');
        $('#box-sms').css('left', left + 'px');
        $('#box-sms').show();
    }
}
//---------------------------------------------------------------------------------
function mostrarLightboxReporte(idAbonado){
    $('#texto-reporte').html('');
    $('#telefono-movil').val('');
    $('#texto-reporte').val('');
    if($('#box-reporte').is(":visible")){
            $('#box-reporte').hide();
    }else{
        var position = $('#envia-reporte-' +  idAbonado).position();
        var top = position.top + 28;
        var left = position.left - 110;
        $('#box-reporte').css('top', top + 'px');
        $('#box-reporte').css('left', left + 'px');
        $('#idAbonado').val(idAbonado);
        $('#box-reporte').show();
    }
}
//---------------------------------------------------------------------------------
function ocultarLightboxSms(){
    $('#texto-sms').html('');
    $('#telefono-movil').val('');
    if($('#box-sms').is(":visible")){
        $('#result-send-sms')
                .html('<a id="button-send-sms" href="#" onclick="sendSMS(); return false;">Enviar</a> <a id="button-send-sms" href="#" onclick="guardarDatosProporcionadosLlamada(); return false;">Guardar Datos</a>');

        $('#box-sms').hide();
    }
}
//---------------------------------------------------------------------------------
function ocultarLightboxReporte(){
    $('#texto-reporte').html('');
    $('#telefono-movil').val('');
    if($('#box-reporte').is(":visible")){
        $('#result-send-reporte')
                .html('<a id="button-send-reporte" href="#" onclick="sendReporte(); return false;">Enviar</a>');
        $('#box-reporte').hide();

    }
}
//---------------------------------------------------------------------------------
function mostrarLightboxRoutes(idAbonado, tipoCalle, calle, portal, codigoPostal,localidad,provincia){
    $('#origen_ruta').html('');
    $('#destino_ruta').val('');
    if($('#box-routes-maps').is(":visible")){
            $('#box-routes-maps').hide();
    }else{
        var destino = '';
        if(destino){
            destino = tipoCalle;
        }
        if(calle != ''){
            if(destino != ''){
                destino += ' ' + calle;
            }else{
                destino = calle;
            }
        }
        if(portal != ''){
            if(destino != ''){
                destino += ' ' + portal;
            }
        }
        if(codigoPostal != ''){
            if(destino != ''){
                destino += ',  ' + codigoPostal;
            }else{
                destino = codigoPostal;
            }
        }
        if(localidad != ''){
            if(destino != ''){
                destino += ',  ' + localidad;
            }else{
                destino = localidad;
            }
        }
        if(provincia != ''){
            if(destino != ''){
                destino += ',  ' + provincia;
            }else{
                destino = provincia;
            }
        }
        $('#destino_ruta').val(destino);
        var position = $('#calcular-ruta-' +  idAbonado).position();
        var top = position.top + 28;
        var left = position.left - 110;
        $('#box-routes-maps').css('top', top + 'px');
        $('#box-routes-maps').css('left', left + 'px');
        $('#box-routes-maps').show();
    }
}
//---------------------------------------------------------------------------------
function calcularRutaMaps(){
    var origen = $('#origen_ruta').val();
    var destino = $('#destino_ruta').val();
    var tipoTransporte = $('#tipoTransporte').val();
    url = URL + 'como_llegar/' + origen + '/' + destino + '/' + tipoTransporte;
    window.open(url, '_blank');
}
//---------------------------------------------------------------------------------
function searchLugaresCercanos(){
    var categoria = $('#categoriaLC').val();
    var direccion = $('#direccionLC').val();
    var radio = $('#radioLC').val();
    var idLlamada = $('#idLlamada').val();

    if(categoria != '' && direccion != '' && radio != ''){
        url = URL + 'lugares-cercanos/' + categoria + '/' + direccion + '/' + radio + '/' + idLlamada;
        window.open(url, '_blank');
    }
}
//---------------------------------------------------------------------------------
// $('#btn_save_informacion_llamante').bind('click', function() {
function guardarInformacionLlamante(){

    if ($('#rolLlamante').val() != '' && $('#sexoLlamante').val() != '') {
        $.ajax({
            url: URL + 'ajax/saveInfoLlamada',
            type: 'post',
            data: {
                idLlamada: $('#idLlamada').val(),
                sexoLlamante: $('#sexoLlamante').val(),
                rolLlamante: $('#rolLlamante').val()
            },
            success: function(){
                $('#sexoLlamante').prop('disabled',true);
                $('#rolLlamante').prop('disabled',true);
                $('#span_save_informacion_llamante').css('cursor',"default");
                $('#btn_save_informacion_llamante').css('cursor',"default");
                $('#span_save_informacion_llamante').css('background-color','#A9A9A9');
                $('#semaforoInformacionLlamante').removeClass('semaforo-rojo');
                $('#semaforoInformacionLlamante').addClass('semaforo-verde');
            }

        });
        return false;
    }
    else {
      alert("Debe seleccionar un elemento distinto de Sexo y/o Tipo Consulta");
      return false;
    }
}
//---------------------------------------------------------------------------------
function saveOfrecimientoSeguro(){
    var respuesta = $("#respuestaOfrecimiento").val()
    $.ajax({
        url: URL + 'ajax/saveOfrecimientoSeguro',
        type: 'post',
        dataType: 'json',
        data: {
            idLlamada: $('#idLlamada').val(),
            idPromocion: $('#idPromocion').val(),
            sexo: $("input:radio[name=sexo]:checked").val(),
            respuesta: respuesta
        },
        success: function(json){
            if(  (respuesta == 'Aceptado') &&  (json['respuesta'] == 'true') ) {
                $('#form_promocion_ofrecimiento').hide();
                $('#datos-seguros-marsh').show();
            } else if ((respuesta != 'Aceptado') && (json['respuesta'] == 'true') ){
                $('#form_promocion_ofrecimiento').hide();
                $('#body-seguros-marsh').html('Los datos se han guardado satisfactoriamente.');
            } else {
                $('#body-seguros-marsh').html('Se produjo un error, vuelvelo a intentar más tarde.');
            }
        }
    });
}
//---------------------------------------------------------------------------------
function saveDatosClientesSeguro() {
    $.ajax({
        url: URL + 'ajax/saveDatosClienteSeguro',
        type: 'post',
        dataType: 'json',
        data: $('#datos-seguros-marsh').serialize(),
        success: function(json) {
            if(json.respuesta == 'true'){
                $('#datos-seguros-marsh').hide();
                $('#body-seguros-marsh').html('Los datos se han guardado satisfactoriamente.');
            } else {
                $('#body-seguros-marsh').html('Se produjo un error, vuelvelo a intentar más tarde.');
            }
        }
    });
}
//---------------------------------------------------------------------------------
function validFormLlamadaVuelta(){

    var camposVacios = 0;

    $('input:text[required]').each(function(){
        if($(this).val() == ''){
            $(this).addClass('error');
            camposVacios++;
        } else
            $(this).removeClass('error');
    })
    if(camposVacios == 0){

        $('#saveFormLlamadaVuelta').attr("onclick", "return false;") ;
        $('#saveFormLlamadaVuelta').css('background','#1a191a');
        $('#info-save').html();
        $('#info-save').removeClass();

        $.ajax({
            url: URL + 'ajax/valid_llamada_vuelta',
            type: 'post',
            dataType: 'json',
            data: $('#informacion-cliente ,#informacion-busqueda').serialize(),
            success: function(json) {
                if(json.ok == 'true') {
                    $('#info-save').html(json.html);
                    $('#info-save').removeClass('message-success');
                    $('#info-save').addClass('message-success');
                }
                else {
                    $('#info-save').html(json.html);
                    $('#info-save').removeClass('message-success');
                    $('#info-save').addClass('message-error');
                }
            }
        });
    } else {
        $('#info-save').html('Debe completar los campos requeridos.');
        $('#info-save').addClass('message-error');
    }
}
//---------------------------------------------------------------------------------
function sendSMS(){
    $.ajax({
        url: URL + 'ajax/send_sms',
        type: 'post',
        data: $('#formulario-sms').serialize(),
        success: function(html){
            $('#result-send-sms').html(html);
            $('#formulario-sms a').hide();
            $('#telefono-movil').val('');
            $('#texto-sms').val('');
            setTimeout('ocultarLightboxSms()', 2000);
        }
    });
}
//---------------------------------------------------------------------------------
function sendReporte(){
    $.ajax({
        url: URL + 'ajax/enviarReporte',
        type: 'post',
        data: $('#formulario-reporte').serialize(),
        success: function(html){
            $('#result-send-reporte').html(html);
            $('#formulario-reporte a').remove();
            $('#telefono-reporte').val('');
            $('#texto-reporte').val('');
            setTimeout('ocultarLightboxReporte()', 2000);

        }
    });
}
//---------------------------------------------------------------------------------
function guardarDatosProporcionadosLlamada()
{
  $.ajax({
      url: URL + 'ajax/guardarDatosProporcionadosLlamada',
      type: 'post',
      data: $('#formulario-sms').serialize(),
      success: function(html){
          $('#result-send-sms').html(html);
          $('#formulario-sms a').remove();
          $('#telefono-movil').val('');
          $('#texto-sms').val('');
          setTimeout('ocultarLightboxSms()', 2000);
      }
  });
}
//---------------------------------------------------------------------------------
function openCloseForms(){
    $('.formulario').hide();
    $(this).toggle();
}
//---------------------------------------------------------------------------------
 $('#busqueda').keyup(function(event){
    if (event.keyCode == 13){
        $('.search').click();
    }
});
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
