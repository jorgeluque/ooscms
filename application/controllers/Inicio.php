<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

  public function __construct () {

    parent::__construct();

    echar_si_no_login();
		
		$this->load->config('recursos');

  }
  
  public function index() {   

    $this->load->model('articulos_model');
    $idUsuario = $this->session->id;

    $data["email"] = $this->session->email;
    $data["numArticulos"] = $this->articulos_model->countArticulos();
    $data["articulosProgramados"] = $this->articulos_model->countArticulosProgramados();
    $data["numCategorias"] = $this->articulos_model->countCategorias();
    $data["numArticulosCategorias"] = $this->articulos_model->countArticulosCategorias();

    $data["articulosCreados"] = $this->articulos_model->countUsuarioRedactados($idUsuario);
    $data["articulosEditados"] = $this->articulos_model->countUsuarioEditados($idUsuario);
    $data["fechaAcceso"] = $this->session->ultimo_acceso;
    $data["ultimosArticulos"] = $this->articulos_model->getUltimosArticulos();

    $data["title"] = "Menú gestor de fichas 11870.com";
    $data["cabecera"] = "Inicio";
		$data["section"] = "inicio";

    $this->load->view('apartados/inicio', $data);
    
  }

}