<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends CI_Controller {

    public function __construct () {

        parent::__construct();

        echar_si_no_login();

		$this->load->library('grocery_CRUD');
		
		$this->load->config('recursos');
		
		$this->load->model('banners_model');
    
    }

    public function listado () {

        $crud = new Grocery_CRUD();
		$state = $crud->getState();
		
		// Seleccionamos la tabla
		$crud->set_table('xclio_banners');
		$crud->set_primary_key('id');
		
		// Seleccionamos los campos de la tabla
		$crud->columns('imagen_desktop','titulo','tel','activo');
		$crud->add_fields('nombre','imagen_desktop','imagen_mobile','tel','activo');
		$crud->edit_fields('nombre','imagen_desktop','imagen_mobile','tel','activo');
        $crud->set_read_fields('nombre','imagen_desktop','imagen_mobile','tel','activo');
        
        // Mejoramos la visualizacion de la tabla
		$crud->display_as('id', 'ID')->display_as('titulo', 'Título');
		$crud->set_subject('banner');
		
		// Campos obligatorios 
		$crud->required_fields('nombre','imagen_desktop','imagen_mobile','tel');
		
		// Tipos de campo
		$crud->field_type('activo','checkbox');
		
		// Campos especiales
		$crud->set_field_upload("imagen_desktop", "assets/img/uploads/banners");
        $crud->set_field_upload("imagen_mobile", "assets/img/uploads/banners");

        // Callbacks
        $crud->callback_before_insert([$this, 'before_insert_update']);
        $crud->callback_before_update([$this, 'before_insert_update']);
        $crud->callback_after_insert([$this, 'after_insert_update']);
        $crud->callback_after_update([$this, 'after_insert_update']);
		
		// Deshabilitamos acciones
		$crud->unset_print();
        
        // Ejecutamos la salida
		$data["crud"] = $crud->render(); 
        $data["title"] = "Listado de banners 11870.com";
		$data["cabecera"] = "Listado de Banners";
		$data["section"] = "tabla-banners";
		$this->load->view('apartados/vista_tablas', $data);

    }

    public function before_insert_update ($post_array) {

        // Controlamos los checkbox
        if (!isset($post_array['activo'])) $post_array['activo'] = 0;  

        return $post_array;

    }

    public function after_insert_update ($post_array, $primary_key) {

        // Si el banner va a quedar activo
        if ($post_array["activo"] == 1) {

            // Desactivamos el resto de banners
            $this->banners_model->desactivar_otros_banners($primary_key);

        }

    }

}