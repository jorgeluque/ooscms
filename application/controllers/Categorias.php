<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends CI_Controller {

    public function __construct () {

        parent::__construct();

		echar_si_no_login();

		$this->load->library('grocery_CRUD');
		
		$this->load->config('recursos');
    
    }

    public function listado () {

        $crud = new Grocery_CRUD();
		$state = $crud->getState();
		
		// Seleccionamos la tabla
		$crud->set_table('xclio_categorias');
		$crud->set_primary_key('id');
		
		// Seleccionamos los campos de la tabla
		$crud->columns('imagen','categoria','url_amigable');
		$crud->add_fields('categoria','url_amigable','seo_titulo','seo_descripcion','cabecera','pie','imagen','seo_alt_imagen');
		$crud->edit_fields('categoria','url_amigable','seo_titulo','seo_descripcion','cabecera','pie','imagen','seo_alt_imagen','modificado');
		$crud->set_read_fields('categoria','cabecera','pie','url_amigable','seo_titulo','imagen','seo_alt_imagen','creado','modificado');

		// Mejoramos la visualizacion de la tabla
        $crud->display_as('id', 'ID')->display_as('categoria','Categoría')->display_as('url_amigable','URL Amigable');
		$crud->display_as('seo_titulo','SEO Título')->display_as('seo_descripcion','SEO Descripción');
		$crud->display_as('seo_alt_imagen','SEO Alt Imagen');
		$crud->set_subject('categoría');
		
		// Campos obligatorios 
		$crud->required_fields('categoria','url_amigable','seo_titulo');
		
		// Tipos de campo
		$crud->field_type('modificado', 'hidden', time());
		$crud->field_type('modificado', 'hidden', time());
		
		// Campos especiales
		$crud->set_field_upload("imagen", "assets/img/uploads/categorias");
		
		// Deshabilitamos acciones
		$crud->unset_print();
		
		// Ejecutamos la salida
		$data["crud"] = $crud->render(); 
        $data["title"] = "Listado de categorías 11870.com";
		$data["cabecera"] = "Listado de Categorías";
		$data["section"] = "tabla-categorias";
		$this->load->view('apartados/vista_tablas', $data);

    }

}