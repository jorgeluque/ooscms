<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function __construct () {

    parent::__construct();

    $this->load->config('recursos');

  }
  
  public function index() {
    //si el usuario no está loguaeado se muestra el formulario de login
    if(empty($this->session->userdata('login')))
    {
      $data["title"] = "Login";
      $data["ficheros_js"] = [recurso("login_js")];
      $this->load->view('apartados/login', $data);
    } 
    //Si está logueado, redireccionamos al backend.
    else
    {
      redireccion(base_url());
    }
  }
  
  public function validar_login() {
    $this->load->library('encrypt');

    $html = '';
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    

    if($email && $password) {
      $email = strtolower($email);

      $this->load->model('usuarios_model');
      
      //Comprobamos si éxiste un usuario con dicho email
      if ($usuario = $this->usuarios_model->getEmail($email)) { 
        //Comprobamos si la contraseña introducida es la mísma que la guardata en la BD.
        if( $usuario->password == $password ) { 
          inicioSesion($usuario);
          $this->usuarios_model->updateUltimoAcceso($usuario->id);
          $html = 'ok';
        }
        else
          $html = 'La contraseña es incorrecta';
      }
      else
        $html = 'El usuario introducido no éxiste.';

    }
    else
      $html = 'Los datos están vacíos';

    echo $html;
  }

  public function validar_email() {
    $json = array();
    if($email = $this->input->post('email')) {
      $email = strtolower($email);

      $this->load->model('usuarios_model');
      
      //Comprobamos si éxiste un usuario con dicho email
      if ($usuario = $this->usuarios_model->getEmail($email)) {
          $json['html'] = 'ok';
          $json['usuario'] = $usuario->idUsuario;
      }
      else
        $json['html'] = 'El usuario introducido no éxiste.';
    }
    else
      $json['html'] = 'Los datos están vacíos';

    echo json_encode($json);
  }



  function nueva_password($usuario = '') {
    if($usuario) {
      $datos['usuario'] = $usuario;
      $this->load->view('apartados/nueva_password',$datos);
    }
    else {
      $this->index();
    }
  }


  function cerrar_sesion() {
    $this->session->sess_destroy();
    redireccion(base_url('login'));
  }


  function resetear_password() {
    $this->load->view('apartados/resetear_password');
  }


}