<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articulos extends CI_Controller {

    public function __construct () {

        parent::__construct();

		echar_si_no_login();

		$this->load->library('grocery_CRUD');
		
		$this->load->config('recursos');
    
    }

    public function listado () {

        $crud = new Grocery_CRUD();
		$state = $crud->getState();
		
		// Seleccionamos la tabla
		$crud->set_table('xclio_articulos');
		$crud->set_primary_key('id');
		
		// Seleccionamos los campos de la tabla
		$crud->columns('imagen_1','titulo','url_amigable','fecha_publicacion');
		$crud->add_fields('id_usuario_creador','id_usuario_modificacion','titulo','id_categoria','url_amigable','fecha_publicacion','seo_titulo','seo_descripcion','contenido_1','imagen_1','seo_alt_imagen_1','contenido_2','imagen_2','seo_alt_imagen_2','telefonos');
		$crud->edit_fields('id_usuario_modificacion','titulo','id_categoria','url_amigable','fecha_publicacion','seo_titulo','seo_descripcion','contenido_1','imagen_1','seo_alt_imagen_1','contenido_2','imagen_2','seo_alt_imagen_2','modificado','telefonos');
		$crud->set_read_fields('id_usuario_creador','id_usuario_modificacion','titulo','id_categoria','url_amigable','fecha_publicacion','seo_titulo','seo_descripcion','contenido_1','imagen_1','seo_alt_imagen_1','contenido_2','imagen_2','seo_alt_imagen_2','creado','modificado','telefonos');

		// Mejoramos la visualizacion de la tabla
		$crud->display_as('id', 'ID')->display_as('titulo', 'Título')->display_as('url_amigable','URL Amigable');
		$crud->display_as('fecha_ultima_edicion','Última Edición')->display_as('id_categoria','Categoría');
		$crud->display_as('seo_titulo','SEO Título')->display_as('seo_descripcion','SEO Descripción');
		$crud->display_as('fecha_publicacion','Fecha Publicación')->display_as('seo_alt_imagen_1','SEO Alt Imagen 1');
		$crud->display_as('seo_alt_imagen_2','SEO Alt Imagen 2');
		$crud->set_subject('artículo');
		
		// Campos obligatorios 
		$crud->required_fields('titulo','url_amigable','seo_titulo','seo_descripcion','fecha_publicacion');
		
		// Tipos de campo
		$crud->field_type('clave','password');
		$crud->field_type('id_usuario_creador', 'hidden', $this->session->id);
		$crud->field_type('id_usuario_modificacion', 'hidden', $this->session->id);

		// Tipos de campo segun el estado
		if ($state == "add" || $state == "insert_validation") {

			$crud->field_type('telefonos', 'hidden', '[]');

		}
		elseif ($state == "edit" || $state == "update_validation") {

			$crud->field_type('modificado', 'hidden', time());
			$crud->field_type('telefonos', 'hidden');

		}
		if ($state == "read") {

			$crud->field_type('telefonos', 'text');

		}
		
		// Campos especiales
		$crud->set_relation("id_categoria", "xclio_categorias", "categoria");
		$crud->set_field_upload("imagen_1", "assets/img/uploads/articulos");
		$crud->set_field_upload("imagen_2", "assets/img/uploads/articulos");
		
		// Deshabilitamos acciones
		$crud->unset_print();
		
		// Ejecutamos la salida
		$data["crud"] = $crud->render(); 
        $data["title"] = "Listado de artículos 11870.com";
		$data["cabecera"] = "Listado de Artículos";
		$data["section"] = "tabla-articulos";
		$data["ficheros_js"] = [recurso("articulos_js")];
		$this->load->view('apartados/vista_tablas', $data);

    }

}