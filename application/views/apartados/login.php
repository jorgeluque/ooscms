<!-- LOGIN -->
<?php
    $title = "Login gestor de fichas 11870.com";
    $this->load->view('elementos/head');
?>

  <body>

    <section id="login-section">
      <div class="container">
      <div class="login">
    
        <h1 class="center">INICIAR SESIÓN</h1>

        <form action="<?= base_url('login/validar_login'); ?>" method="post" name="login" id="login_usuario">
            <div class="hr">

              <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email"  name="login[email]" required="required">
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Contraseña</label>
                <input type="password" class="form-control" id="password" name="login[password]" required="required">
              <a href="<?= base_url('login/resetear_password'); ?>">¿Olvidaste tu contraseña?</a>
              </div>
            </div>
            <div id="error-login"></div>
            <button type="submit" class="btn btn-primary">Entrar</button>
        </form>

      </div>
      </div>
    </section>
  
    <?php $this->load->view('elementos/scripts') ?>
  
  </body>

</html>