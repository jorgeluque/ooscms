<!-- MENÚ -->
<?php $this->load->view("elementos/head"); ?>

  <body>

    <?php $this->load->view("elementos/menu_lateral"); ?>

      <div id="content">

        <section id="<?= $section; ?>">

            <?php $this->load->view('elementos/header') ?>
            
            <?= $crud->output ?>
        
        </section>
        
      </div>
  
    <?php $this->load->view('elementos/scripts') ?>

  </body>

</html>