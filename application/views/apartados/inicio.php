<!-- MENÚ -->
<?php $this->load->view('elementos/head'); ?>

<body>

	<?php $this->load->view('elementos/menu_lateral') ?>

	<div id="content">

		<section id="<?= $section; ?>">
			
			<?php $this->load->view('elementos/header') ?>

			<div class="resumen-box">
				<h3 style="text-align: center">Artículos</h3>
				<p>Artículos totales:
					<?= $numArticulos ?>
				</p>
				<p>Artículos programados:
					<?= $articulosProgramados ?>
				</p>
				<p>Número de categorías:
					<?= $numCategorias ?>
				</p>
			</div>


			<div class="resumen-box">
				<h3 style="text-align: center">Categorías</h3>

				<?php foreach ($numArticulosCategorias as $row): ?>
                    <p>Artículos en
                        <?= $row['categoria']?>:
                        <?= $row['numCategoria']?>
                    </p>
                <?php endforeach; ?>
			</div>


			<div class="resumen-box">
				<h3 style="text-align: center">Tus estadísticas</h3>
				<p>Artículos creados:
					<?= $articulosCreados?>
				</p>
				<p>Última fecha de acceso:
					<?= $fechaAcceso?>
				</p>
				<p>Has sido el último en editar
					<?= $articulosEditados?> artículos</p>
			</div>

			<div class="resumen-box">
				<h3 style="text-align: center">Últimos artículos</h3>

				<?php foreach ($ultimosArticulos as $row): ?>
                    <p>
						<?= $row['titulo']?>
                        <a href="editarArticulo/<?= $row['id']?>">[editar]</a>
                    </p>
				<?php endforeach; ?>
			</div>

		</section>

	</div>

	<?php $this->load->view('elementos/scripts') ?>

</body>

</html>
