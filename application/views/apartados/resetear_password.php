<?php $this->load->view('elementos/head'); ?>

<div style="text-align: center;position: relative;top: 10%;">
  <a href="<?= base_url('login'); ?>"><img src="<?= base_url('assets/img/cara.png'); ?>" ></a>
</div>                
<div style="position: relative;margin: 0 auto; text-align: center; width: 400px;">
    <div class="login">
        <h1 class="center">Restaurar Contraseña</h1>
        <p>Contacta con <a href="mailto:desarrollo@11811.es">desarrollo@11811.es</a> para poder cambiar tu contraseña.</p>
    </div>
</div>
  
<?php $this->load->view('elementos/scripts') ?>

<script type="text/javascript">
$('#form-reset-password').submit(function(event){
  event.preventDefault();
  $('#msg-reset-password').html('');
  $.ajax({
    url: base_url + 'login/validar_email',
    type: 'post',
    dataType: 'json',
    data: $('#form-reset-password').serialize(),
    success: function(json) {
      if(json.html == 'ok')
      {
        url = base_url + "login/nueva_password/" + json.usuario;
        window.location.replace(url);
      }
      else
        $('#msg-reset-password').html('<p class="bg-danger">'+html+'</p>');
    }
  });
});
</script>
