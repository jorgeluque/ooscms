<header id="menu-superior">
    <h1 class="float-left"><?= $cabecera ?></h1>
    <div class="usuario-derecha float-right">
        <p class="email-usuario float-left"><?= $this->session->email ?> </p>
        <p class="cerrar-sesion float-left"><a href="<?= base_url('login/cerrar_sesion') ?>">(cerrar sesión)</a></p>
    </div>
</header>