<nav id="nav-menu">
  <div class="menu-bar">

    <h2><a href="<?= base_url() ?>"><strong>Inicio</strong></a></h2>

    <hr>  

    <h2>Artículos</h2>

    <p><a href="<?= base_url('articulos/listado/add') ?>">Nuevo artículo</a></p>
    <p><a href="<?= base_url('articulos/listado') ?>">Listado de artículos</a></p>

    <hr>

    <h2>Categorías</h2>

    <p><a href="<?= base_url('categorias/listado/add') ?>">Nueva categoría</a></p>
    <p><a href="<?= base_url('categorias/listado') ?>">Listado de categorías</a></p>

    <hr>

    <h2>Banners</h2>

    <p><a href="<?= base_url('banners/listado/add') ?>">Nuevo banner</a></p>
    <p><a href="<?= base_url('banners/listado') ?>">Listado de banners</a></p>
    
  </div>
</nav>
