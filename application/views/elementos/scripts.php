<script type="text/javascript">
    var base_url = '<?php echo base_url() ?>';
    var site_url = '<?php echo site_url() ?>';
</script>

<!-- GROCERY CRUD -->
<?php if (isset($crud)): ?>
    <?php foreach ($crud->js_files as $file): ?>
        <script type="text/javascript" src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>

<?php if ( ! isset($crud) ): ?>
    <script type="text/javascript" src="<?= recurso('jquery_js') ?>" ></script>
<?php endif; ?>
<script type="text/javascript" src="<?= recurso('tether_js') ?>" ></script>
<?php if ( ! isset($crud) ): ?>
    <script type="text/javascript" src="<?= recurso('bootstrap_js') ?>" ></script>
<?php endif; ?>
<script type="text/javascript" src="<?= recurso('md5_js') ?>" ></script>

<!-- Click to call google -->
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion_async.js"></script>

<!-- JS Propios -->
<?php if (isset($ficheros_js)): ?>
    <?php foreach ($ficheros_js as $fichero_js): ?>
        <script type="text/javascript" src="<?php echo $fichero_js; ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>