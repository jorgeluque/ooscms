<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?= $title ?></title>
        <meta name="description" content="Gestor de subidas y modificaciones de fichas empresa 11870.com.">

        <?php $url = base_url(); ?>

        <meta name="viewport" content="width=768, user-scalable=yes">
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('assets/img/favicon.ico') ?>"/>
        
        <?php if (isset($crud)): ?>
            <?php foreach ($crud->css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach; ?>
        <?php endif; ?>
        
        <?php if ( ! isset($crud) ): ?>
            <link rel="stylesheet" href="<?= recurso('bootstrap_css') ?>">
        <?php endif; ?>

        <link rel="stylesheet" href="<?= recurso("style_css") ?>">

    </head>