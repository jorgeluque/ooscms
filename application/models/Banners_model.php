<?php

class Banners_model extends CI_Model {

    public function desactivar_otros_banners ($id_banner) {

        $datos = ["activo" => 0];

        $this->db->where("id != ", $id_banner);
        $this->db->update("banners", $datos);

    }

}

?>