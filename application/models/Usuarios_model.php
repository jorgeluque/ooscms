<?php


class Usuarios_model extends CI_Model
{
  
  public function encrypt_password_callback($data, $primary_key = null)
  {
    $data['password'] = md5($data['password']);
    return $data;
  }


  public function getEmail($email)
  {
    return $this->db->get_where('usuarios', array('email' => $email))->row();
  }


  public function set_password($idUsuario = '', $password = '')
  {
    if($idUsuario && $password)
    {
      $data = array(
        'password' => $password
      );
      $where = 'idUsuario = '.$idUsuario;
      $this->db->update('usuarios', $data, $where);
      return $idUsuario;
    }
    else
    {
      return false;
    }
  }


  public function updateUltimoAcceso($id = '')
  {
    $this->db->set('ultimo_acceso', date("Y-m-d H:i:s"));
    $this->db->where('id', $id);
    $this->db->update('usuarios');
  }

}
