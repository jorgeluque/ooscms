<?php


class Articulos_model extends CI_Model
{
  
  public function countArticulos()
  {
    return $this->db->count_all_results('articulos');
  }

   public function countArticulosProgramados()
  {
      $this->db->where('fecha_publicacion >', date("Y-m-d H:i:s"));
      $result = $this->db->get('articulos');
      return $result->num_rows();
  }


  public function countCategorias()
  {
      return $this->db->count_all_results('categorias');
  }


  public function countArticulosCategorias()
  {
      $this->db->select('categoria, count(id_categoria) as numCategoria');
      $this->db->from('articulos');
      $this->db->join('categorias', 'categorias.id = id_categoria', 'right');
      $this->db->group_by('id_categoria');
      $query = $this->db->get();

      return $query->result_array();
  }


  public function countUsuarioRedactados($id)
  {
      $this->db->where('id_usuario_creador', $id);
      return $this->db->count_all_results('articulos');

  }


  public function countUsuarioEditados($id)
  {
      $this->db->where('id_usuario_modificacion', $id);
      return $this->db->count_all_results('articulos');
  }

  
  public function getUltimosArticulos()
  {
      $this->db->select('*');
      $this->db->order_by('id', 'desc');
      $query = $this->db->get('articulos', 5);
      return $query->result_array();
  }

  public function getPostByID($id)
  {

  }


  public function getPostCategory($id = '')
  {
    
  }


  public function getNombre($idUsuario = '')
  {
    
  }


  public function createNewPost($idUsuario = '')
  {
    
  }

}
