<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['login/(:any)'] = 'login/$1';
$route['login/nueva_password/(:any)'] = 'login/nueva_password/$1';

$route['menu'] = 'menu/index';
$route['index.php/menu/listadoArticulos/add'] = 'menu/listadoArticulos/add';

$route['ajax/(:any)'] = 'ajax/$1';

$route['default_controller'] = 'inicio';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
