<?php
	
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	// Javascript
	$config['recursos']['jquery_js'] = base_url('assets/js/library/jquery.min.js?v=1.10.2');
	$config['recursos']['md5_js'] = base_url('assets/js/library/md5.min.js?v=1.00');
	$config['recursos']['tether_js'] = base_url('assets/js/library/tether.min.js?v=1.00');
	$config['recursos']['bootstrap_js'] = base_url('assets/js/library/bootstrap.min.js?v=3.3.5');
	$config['recursos']['articulos_js'] = base_url('assets/js/articulos.js?v=1.00');
	$config['recursos']['login_js'] = base_url('assets/js/login.js?v=1.00');
	
	// CSS
	$config['recursos']['bootstrap_css'] = base_url('assets/css/bootstrap.min.css?v=3.3.5');
	$config['recursos']['style_css'] = base_url('assets/css/style.css?v=1.00');
	
?>