<?php

  function comprobarSesionIniciada() {
    $CI = & get_instance();
    $sesion = false;
    if (($CI->session->login)){
      $sesion = true;
    }
    return $sesion;
  }
  //-----------------------------------------------------------------
  function echar_si_no_login () {
    if (comprobarSesionIniciada() == false) {
      redireccion(base_url('login'));
    }
  }
  //-----------------------------------------------------------------
  function redireccion ($url) {
		
		header("Location: ".$url);
		die;
		
	}
  //-----------------------------------------------------------------
    function inicioSesion($usuario){
      $CI = & get_instance();
      $CI ->session->set_userdata('login',true);
      $CI ->session->set_userdata('id',$usuario->id);
      $CI ->session->set_userdata('email',$usuario->email);
      $CI ->session->set_userdata('privilegios',$usuario->privilegios);
      $CI ->session->set_userdata('creado',$usuario->creado);
      $CI ->session->set_userdata('ultimo_acceso',$usuario->ultimo_acceso);  
    }
  //-----------------------------------------------------------------
    function validarEmail($email)
    {
      if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email))
      {
          return true;
      }
      else
      {
        return false;
      }
    }
  //-----------------------------------------------------------------
    function stdClassToArray($stdClass){
      $array = json_decode(json_encode($stdClass), true);
      return $array;
    }
    //-----------------------------------------------------------------
    function sanear_string($string)
    {
      //Esta parte se encarga de eliminar cualquier caracter extraño
      $string = str_replace(
          "+",
          '',
          $string
      );
      return $string;
    }
    //-----------------------------------------------------------------
    function quitarEspaciosEnBlanco($cadena)
    {
      $cadena = ltrim(rtrim($cadena));
      $cadena = preg_replace ('/[ ]+/', ' ', $cadena);
      return $cadena;
    }

    //-----------------------------------------------------------------

    function minusculas($string){
      $string= strtolower($string);
      $minusculas = array('á','à','é','è','í','ì','ó','ò','ú','ù','ñ','ç');
      $mayusculas = array('Á','À','É','È','Í','Ì','Ó','Ò','Ú','Ù','Ñ','Ç');
      $stringFinal = str_replace($mayusculas,$minusculas, $string);
        return $stringFinal;
    }
    //-----------------------------------------------------------------

    function omitirTildes($string){
      $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");

      $permitidas= array ("a","e","i","o","u","A","E","I","O","U","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");

      $texto = str_replace($no_permitidas, $permitidas ,$string);
      return $texto;
    }
    //-----------------------------------------------------------------
    function fecha() {
      return date('Y-m-d\TH:i:s');
    }
    //-----------------------------------------------------------------
    function limpiarCadena($texto) {
      $texto = sanear_string($texto);
      $texto = quitarEspaciosEnBlanco(strtolower($texto));
      $texto = quitarPreposicionesYarticulos($texto);
      return $texto;
    }
    //-----------------------------------------------------------------
    function send_email($to = array(), $subject = "", $body = "")
    {
      require("assets/phpmailer/class.phpmailer.php");
      $mail = new PHPMailer();

      //Luego tenemos que iniciar la validación por SMTP:
      $mail->IsSMTP();
      $mail->SMTPAuth = true;
      $mail->Host = "smtp.gmail.com"; // SMTP a utilizar. Por ej. smtp.elserver.com
      $mail->Username = "atencion.usuario@11811.es"; // Correo completo a utilizar
      $mail->Password = "\$Once8Once\$"; // Contraseña
      $mail->Port = 465; // Puerto a utilizar
      $mail->SMTPSecure = "ssl";
      $mail->SMTPDebug  = 1;

      $mail->From = "atencion.usuario@11811.es"; // Desde donde enviamos (Para mostrar)
      $mail->FromName = "Goonce - 11811";

      if(is_array($to))
      {
        foreach ($to as $email => $name) {
          $mail->AddAddress($email, $name);
        }
      }
      else {
        $mail->AddAddress($to);
      }

      $mail->IsHTML(true); // El correo se envía como HTML
      $mail->Subject = $subject; // Este es el titulo del email.
      $mail->Body = $body;

      try
      {
        $mail->send();
        return true;
      }
      catch(Exception $e)
      {
        $html =  'Message could not be sent.';
        $html .= 'Mailer Error: ' . $mail->ErrorInfo;
        return $html;
      }
    }
    //-----------------------------------------------------------------
    function w1250_to_utf8($text) {
    // map based on:
    // http://konfiguracja.c0.pl/iso02vscp1250en.html
    // http://konfiguracja.c0.pl/webpl/index_en.html#examp
    // http://www.htmlentities.com/html/entities/
    $map = array(
        chr(0x8A) => chr(0xA9),
        chr(0x8C) => chr(0xA6),
        chr(0x8D) => chr(0xAB),
        chr(0x8E) => chr(0xAE),
        chr(0x8F) => chr(0xAC),
        chr(0x9C) => chr(0xB6),
        chr(0x9D) => chr(0xBB),
        chr(0xA1) => chr(0xB7),
        chr(0xA5) => chr(0xA1),
        chr(0xBC) => chr(0xA5),
        chr(0x9F) => chr(0xBC),
        chr(0xB9) => chr(0xB1),
        chr(0x9A) => chr(0xB9),
        chr(0xBE) => chr(0xB5),
        chr(0x9E) => chr(0xBE),
        chr(0x80) => '&euro;',
        chr(0x82) => '&sbquo;',
        chr(0x84) => '&bdquo;',
        chr(0x85) => '&hellip;',
        chr(0x86) => '&dagger;',
        chr(0x87) => '&Dagger;',
        chr(0x89) => '&permil;',
        chr(0x8B) => '&lsaquo;',
        chr(0x91) => '&lsquo;',
        chr(0x92) => '&rsquo;',
        chr(0x93) => '&ldquo;',
        chr(0x94) => '&rdquo;',
        chr(0x95) => '&bull;',
        chr(0x96) => '&ndash;',
        chr(0x97) => '&mdash;',
        chr(0x99) => '&trade;',
        chr(0x9B) => '&rsquo;',
        chr(0xA6) => '&brvbar;',
        chr(0xA9) => '&copy;',
        chr(0xAB) => '&laquo;',
        chr(0xAE) => '&reg;',
        chr(0xB1) => '&plusmn;',
        chr(0xB5) => '&micro;',
        chr(0xB6) => '&para;',
        chr(0xB7) => '&middot;',
        chr(0xBB) => '&raquo;',
    );
    return html_entity_decode(mb_convert_encoding(strtr($text, $map), 'UTF-8', 'ISO-8859-2'), ENT_QUOTES, 'UTF-8');
}
    //-----------------------------------------------------------------
    function camelCase($str, array $noStrip = [])
    {
            // non-alpha and non-numeric characters become spaces
            $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
            $str = trim($str);
            // uppercase the first character of each word
            $str = ucwords($str);
            $str = str_replace(" ", "", $str);
            $str = lcfirst($str);

            return $str;
    }

    //-----------------------------------------------------------------
    function eliminarVariablesSession()
    {
      if(isset($_SESSION['busqueda']))
        unset($_SESSION['busqueda']);
      if(isset($_SESSION['sexoLlamante']))
        unset($_SESSION['sexoLlamante']);
      if(isset($_SESSION['rolLlamante']))
        unset($_SESSION['rolLlamante']);
      if(isset($_SESSION['tipoBusqueda']))
        unset($_SESSION['tipoBusqueda']);
      if(isset($_SESSION['idBusqueda']))
        unset($_SESSION['idBusqueda']);
    }
    //-----------------------------------------------------------------
    function recurso ($nombre_recurso) {
      
      $ci = & get_instance();
      
      $recursos = $ci->config->item("recursos");

      if (isset($recursos[$nombre_recurso])) $recurso = $recursos[$nombre_recurso];
      else $recurso = "";

      return $recurso;
      
    }
    //-----------------------------------------------------------------

  ?>
